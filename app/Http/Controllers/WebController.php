<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response as IlluminateResponse;
use Illuminate\Http\Request;

class WebController extends Controller
{
    public function json(Request $request)
    {
        $response = [];
        $total = 0;

        $orderBy = 'capcode';

//        if($request->input('order.0.column') == 1) {
//            $orderBy = 'name';
//        }

        $query = [
            'order_by' => $orderBy,
            'sort' => $request->input('order.0.dir'),
            'limit' => $request->input('length'),
            'offset' => $request->input('start'),
            'fields' => 'capcode,description,changed,region{name,number},vehicle_type{name},sector{name},fire_station{name},discipline{name}'
        ];

        if($request->filled('search.value')) {
            data_set($query,'q.0.fz',$request->input('search.value'));

            data_set($query,'fire_stations.0.fz',$request->input('search.value'));
//            data_set($query,'sectors.0.fz',$request->input('search.value'));
        }

        $apiRequest = $this->api('p2000/capcodes', 'get', $query);
        if(data_get($apiRequest,'httpStatusCode') == 200) {

            $items = data_get($apiRequest, 'response');

            if(data_get($items,'data')) {

                foreach(data_get($items,'data') as $item) {

                    $response[] = [
                        'capcode' => data_get($item,'capcode'),
                        'discipline' => data_get($item,'discipline.name'),
                        'sector' => data_get($item,'sector.name'),
                        'fire_station' => data_get($item,'fire_station.name'),
                        'region' => data_get($item,'region.name'),
                        'description' => data_get($item,'description'),
                        'changed' => (data_get($item,'changed') ? $this->dateFromMicrotimestamp(data_get($item,'changed')) : ''),
                    ];
                }
            }

            $total = data_get($items, 'summary.total_count');
        }

        return response()->json([
//            'draw' => 1,
//            'recordsTotal' => $total,
            'recordsFiltered' => $total,
            'data' => $response
        ]);
    }

    public function index(Request $request)
    {
        return view('index');
    }

    public function dateFromMicrotimestamp($microtimestamp, $format = null)
    {
        if($microtimestamp) {
            return \Carbon\Carbon::createFromTimestamp($microtimestamp/1000)->formatLocalized(($format ? $format : '%d-%m-%Y %H:%M'));
        }
    }
}
